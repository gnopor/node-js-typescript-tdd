import request from "supertest";
import { describe, it } from "@jest/globals";

import app from "../../src/server";

describe("dashboard routes", () => {
    it("/dashboard responds with 200.", (done) => {
        request(app).get("/dashboard").expect(200, done);
    });
});
