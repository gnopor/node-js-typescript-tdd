import request from "supertest";
import { describe, it } from "@jest/globals";

import app from "../../src/server";

describe("auth routes", () => {
    it("/auth responds with 200.", (done) => {
        request(app).get("/auth").expect(200, done);
    });
});
