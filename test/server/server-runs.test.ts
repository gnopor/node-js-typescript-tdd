import request from "supertest";
import { describe, it } from "@jest/globals";

import app from "../../src/server";

describe("server checks", () => {
    it("server is created without error.", (done) => {
        request(app).get("/").expect(200, done);
    });
});
