import app from "./server";

const PORT = process.env.PORT || 8000;

async function main() {
    try {
        app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
    } catch (error) {
        console.log(error);
    }
}

main();
