import { Router } from "express";

// eslint-disable-next-line new-cap
const router = Router();

router.get("/", (req, res) => {
    res.status(200).send("dashboard");
});

export default router;
