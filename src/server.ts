// eslint-disable-next-line spaced-comment

import express from "express";
import * as http from "http";
import dotenv from "dotenv";
dotenv.config();

import router from "./routes/index";

const app = express();
const httpServer = http.createServer(app);

app.use(router);

app.get("/", (req, res) => {
    res.send("hello world!");
});

process.on("SIGTERM", () => {
    console.info("SIGTERM signal received.");
    httpServer.close(() => {
        console.log("Http server closed.");

        process.exit(0);
    });
});

export default app;
